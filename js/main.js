document.addEventListener('DOMContentLoaded', function() {
    const menuItem = document.querySelector('.js-lectures-menu');
    menuItem.addEventListener('click', function() {
        document.querySelector('.lectures-menu__list').classList.toggle('opened');
        document.querySelector('.js-lectures-menu').classList.toggle('active');
    });

    // открытие закрытие бокового меню
    const burger = document.querySelector('.burger');
    burger.addEventListener('click', function() {
        document.querySelector('.side-panel').classList.add('opened');
    });

    const logoBurger = document.querySelector('.logo-burger');
    logoBurger.addEventListener('click', function() {
        document.querySelector('.side-panel').classList.remove('opened');
    })
});

const swiper = new Swiper(".swiper", {
        
        navigation: {
            nextEl: ".level-arrow-next",
            prevEl: ".level-arrow-prev",
        },
        draggable: true,
        breakpoints: {
            325: {
                slidesPerView: 1,
                spaceBetween: 11,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 11,
            },
            730: {
                slidesPerView: 2,
                spaceBetween: 11,
            },
            960: {
                slidesPerView: 3,
                spaceBetween: 11,
            },
            1150: {
                slidesPerView: 3,
                spaceBetween: 11,
            },
            1400: {
                slidesPerView: 4,
                spaceBetween: 11,
            }
        },
    });

const baseLvlTitles = document.querySelectorAll('#js-base-level #lesson-title');
baseLvlTitles.forEach((element) => {
    element.style.textTransform = 'uppercase';
});

const baseLvlText = document.querySelectorAll('#js-base-level #lesson-subtitle');
baseLvlText.forEach((element) => {
    if (element.innerHTML.length >= 20) {
        element.innerHTML = element.innerHTML.slice(0, 20) + '...';
    }
});